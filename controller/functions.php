<?php

class Common_controller {
    public $default_res_arr = ['result' => 'false', 'message' => 'Something went wrong!!'];

    function __construct(&$req_data) {
        $this->verify_token($req_data);
    }

    function verify_token(&$req_data) {
        $res_data = $this->api_call('/me', $req_data['token']);
        if($res_data['status'] != STATUS_OK) {
            throw new Exception('Error in response');
        } else {
            $req_data['user_data'] = $res_data['response'];
        }
    }

    function api_call($api_endpoint, $token = '', $query = array(), $method = 'GET', $headers = []) {
        $api_query = $query;
        $url = API_URL.$api_endpoint;
        if (!is_null($query) && in_array($method, array('GET', 	'DELETE'))) $url = $url . "?" . http_build_query($query);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        $request_headers = [];
        $token ? ($request_headers[] = 'Authorization: '.$token) : '';
        if($headers) {
            $request_headers = array_merge($request_headers, $headers);
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
            if (is_array($query)) $api_query = http_build_query($query);
            curl_setopt ($curl, CURLOPT_POSTFIELDS, $api_query);
        }

        $response = curl_exec($curl);
        $error_number = curl_errno($curl);
        $error_message = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if($error_number) {
            return ['status' => $httpcode, 'error' => $error_message];
        } else {
            $response = preg_split("@\r?\n\r?\nHTTP/@u", $response);
            $response = (count($response) > 1 ? 'HTTP/' : '').array_pop($response);
            $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
            $final_res = !empty($response[1]) ? json_decode($response[1], TRUE) : [];
            return ['status' => $httpcode, 'response' => $final_res];
        }
    }

    function get_user_data($request_data) {
        $response = $this->default_res_arr;
        if(!empty($request_data['user_data'])) {
            $response['result'] = $response['message'] = 'true';
            $response['user_data'] = $request_data['user_data'];
        }
        return $response;
    }

    function show_author_list($req_data) {
        $response = $this->default_res_arr;
        try {
            $page_number = !empty($req_data['page_number']) ? $req_data['page_number'] : '1';
            $limit = !empty($req_data['limit']) ? $req_data['limit'] : PER_PAGE_RECORD;
            $query = !empty($req_data['query']) ? $req_data['query'] : '';
            $param_arr = [
                'page' => $page_number,
                'limit' => $limit,
                'orderBy' => 'id',
                'direction' => 'DESC',
                'query' => $query,
            ];
            $res_data = $this->api_call('/authors', $req_data['token'], $param_arr);
            if(!empty($res_data['status']) && $res_data['status'] == STATUS_OK) {
                if(!empty($res_data['response']['items'])) {
                    $res_data = $res_data['response'];
                    $pagination_html = $this->prepare_pagination_html($res_data, 'showAuthorList');
                    $table_html = '';
                    foreach($res_data['items'] as $author) {
                        $dob = $author['birthday'] ? date('Y-m-d', strtotime($author['birthday'])) : '-';
                        $table_html .= '
                            <tr>
                                <th scope="row">'.$author['id'].'</th>
                                <td>'.$author['first_name'].' '.$author['last_name'].'</td>
                                <td>'.$author['place_of_birth'].'</td>
                                <td>'.$author['gender'].'</td>
                                <td>'.$dob.'</td>
                                <td>
                                    <a href="'.SITE_URL.'view/author_info.php?author_id='.$author['id'].'" class="btn btn-primary"><i class="far fa-eye"></i></a>
                                    <button type="button" class="btn btn-danger" onclick="deleteAuthor('.$author['id'].')"><i class="far fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        ';
                    }
                } else {
                    $pagination_html = '';
                    $table_html = '<tr><td colspan="6">No records found</td></tr>';
                }
                $response['result'] = 'true';
                $response['message'] = 'Success';
                $response['paginationHtml'] = $pagination_html;
                $response['tableHtml'] = $table_html;
            } else {
                throw new Exception('Error in get author api..');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function prepare_pagination_html($api_response, $function_name) {
        $total_pages = $api_response['total_pages'];
        $current_page = $api_response['current_page'];
        $pagination_html = '';
        if($current_page >= 2) {
            $pagination_html .= '<li class="page-item" onclick="'.$function_name.'('.($current_page-1).')">
                    <a class="page-link" href="javascript:void(0)">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>';
        }

        for ($i=1; $i <= $total_pages; $i++) {
            $active_class = '';
            if ($i == $current_page) {
                $active_class = 'active';
            }
            $pagination_html .= '<li class="page-item '.$active_class.'" onclick="'.$function_name.'('.$i.')"><a class="page-link" href="#">'.$i.'</a></li>';
        };

        if($current_page < $total_pages) {
            $pagination_html .= '<li class="page-item" onclick="'.$function_name.'('.($current_page+1).')">
                    <a class="page-link" href="javascript:void(0)">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>';
        }
        return $pagination_html;
    }

    function verify_author_books($req_data) {
        $response = $this->default_res_arr;
        try {
            if(!empty($req_data['author_id'])) {
                $author_id = $req_data['author_id'];
                $res_data = $this->api_call('/authors/'.$author_id, $req_data['token']);
                if(!empty($res_data['status']) && $res_data['status'] == STATUS_OK) {
                    if(empty($res_data['response']['books'])) {
                        $can_delete = true;
                    } else {
                        $can_delete = false;
                    }
                    $response['result'] = 'true';
                    $response['message'] = 'Success';
                    $response['canDelete'] = $can_delete;
                } else {
                    throw new Exception('Error in get author api..');
                }
            } else {
                throw new Exception('Author id required!!');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function delete_author($req_data) {
        $response = $this->default_res_arr;
        try {
            if(!empty($req_data['author_id'])) {
                $author_id = $req_data['author_id'];
                $res_data = $this->api_call('/authors/'.$author_id, $req_data['token'], [], 'DELETE');
                if(!empty($res_data['status']) && $res_data['status'] == STATUS_DELETE) {
                    $response['result'] = 'true';
                    $response['message'] = 'Success';
                } else {
                    throw new Exception('Error in delete author api..');
                }
            } else {
                throw new Exception('Author id required!!');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function get_author_name_list($req_data){
        $response = $this->default_res_arr;
        try {
            $page_number = !empty($req_data['page_number']) ? $req_data['page_number'] : '1';
            $limit = !empty($req_data['limit']) ? $req_data['limit'] : PER_PAGE_RECORD;
            $search_keyword = !empty($req_data['search_keyword']) ? $req_data['search_keyword'] : '';
            $param_arr = [
                'page' => $page_number,
                'limit' => $limit,
                'orderBy' => 'id',
                'direction' => 'DESC',
                'query' => $search_keyword,
            ];
            $res_data = $this->api_call('/authors', $req_data['token'], $param_arr);
            if(!empty($res_data['status']) && $res_data['status'] == STATUS_OK) {
                $author_name_list = array();
                if(!empty($res_data['response']['items'])) {
                    $res_data = $res_data['response'];
                    foreach($res_data['items'] as $key => $author) {
                        $author_name_list[$key]['id'] = $author['id'];
                        $author_name_list[$key]['text'] = $author['first_name'].' '.$author['last_name'];
                    }
                }
                $response['result'] = 'true';
                $response['message'] = 'Success';
                $response['authorNameList'] = $author_name_list;
            } else {
                throw new Exception('Error in get author api..');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function create_new_book($req_data){
        $response = $this->default_res_arr;
        try {
            $param_arr = [
                'author' => array("id"=>(int)$req_data['author_id']),
                'title' => $req_data['title'],
                'release_date' => date("Y-m-d", strtotime($req_data['release_date'])).'T00:00:00.001Z',
                'description' => $req_data['description'],
                'isbn' => $req_data['isbn'],
                'format' => $req_data['format'],
                'number_of_pages' => (int)$req_data['number_of_pages'],
            ];
            $res_data = $this->api_call('/books', $req_data['token'], json_encode($param_arr),'POST', ['Content-Type: application/json']);
            if(!empty($res_data['status']) && $res_data['status'] == STATUS_OK) {
                $response['result'] = 'true';
                $response['message'] = 'Book created successfully';
                $response['response'] = $res_data['response'];
            } else {
                throw new Exception('Error in create book api..');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function create_new_author($req_data){
        $response = $this->default_res_arr;
        try {
            $param_arr = [
                'first_name' => $req_data['first_name'],
                'last_name' => $req_data['last_name'],
                'birthday' => date("Y-m-d", strtotime($req_data['birthday'])).'T00:00:00.000Z',
                'biography' => $req_data['biography'],
                'gender' => $req_data['gender'],
                'place_of_birth' => $req_data['place_of_birth'],
            ];
            $res_data = $this->api_call('/authors', $req_data['token'], $param_arr,'POST');
            if(!empty($res_data['status']) && $res_data['status'] == STATUS_OK) {
                $response['result'] = 'true';
                $response['message'] = 'Author create successfully';
                $response['response'] = $res_data['response'];
            } else {
                throw new Exception('Error in create author api..');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function get_author_details($req_data) {
        $response = $this->default_res_arr;
        try {
            if(!empty($req_data['author_id'])) {
                $author_id = $req_data['author_id'];
                $res_data = $this->api_call('/authors/'.$author_id, $req_data['token']);
                if(!empty($res_data['status']) && $res_data['status'] == STATUS_OK) {
                    $response['result'] = 'true';
                    $response['message'] = 'Success';
                    $response['authorData'] = $res_data['response'];
                    $response['authorData']['birthday'] = $res_data['response']['birthday'] ? date('Y-m-d', strtotime($res_data['response']['birthday'])) : '-';
                    $book_table_html = '';
                    if(!empty($res_data['response']['books'])) {
                        foreach($res_data['response']['books'] as $book) {
                            $date = $book['release_date'] ? date('Y-m-d H:i:s', strtotime($book['release_date'])) : '-';
                            $book_table_html .= '
                            <tr>
                                <th scope="row">'.$book['id'].'</th>
                                <td>'.$book['title'].'</td>
                                <td>'.$book['description'].'</td>
                                <td>'.$book['isbn'].'</td>
                                <td>'.$book['format'].'</td>
                                <td>'.$book['number_of_pages'].'</td>
                                <td>'.$date.'</td>
                                <td>
                                    <button type="button" class="btn btn-danger" onclick="deleteBook('.$book['id'].')"><i class="far fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        ';
                        }
                    } else {
                        $book_table_html = '<tr><td colspan="8">No records found</td></tr>';
                    }
                    $response['tableHtml'] = $book_table_html;
                } else {
                    throw new Exception('Error in get author api..');
                }
            } else {
                throw new Exception('Author id required!!');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }

    function delete_book($req_data) {
        $response = $this->default_res_arr;
        try {
            if(!empty($req_data['book_id'])) {
                $book_id = $req_data['book_id'];
                $res_data = $this->api_call('/books/'.$book_id, $req_data['token'], [], 'DELETE');
                if(!empty($res_data['status']) && $res_data['status'] == STATUS_DELETE) {
                    $response['result'] = 'true';
                    $response['message'] = 'Success';
                } else {
                    throw new Exception('Error in delete author api..');
                }
            } else {
                throw new Exception('Book id required!!');
            }
        } catch(\Exception $ex) {
            $response['dev_message'] = $ex->getMessage();
        }
        return $response;
    }
}
