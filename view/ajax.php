<?php
ob_start();
header("Access-Control-Allow-Origin: *");

include_once '../config/config.php';

ob_clean();

try {
    $is_bad_req = 0;
    $response = array('result' => 'fail', 'message' => 'Opps! Bad request call!');
    if (!empty($_POST['request_method'])) {
        include_once ABS_PATH . '/controller/functions.php';

        $security_token = '';
        if (!empty($_POST['token'])) {
            $security_token = $_POST['token'];
        } else {
            $headerDataArr = apache_request_headers();
            $security_token = !empty($headerDataArr['Authentication']) ? $headerDataArr['Authentication'] : '';
        }

        /* Confirm if cookie is set */
        if(empty($_COOKIE['authData'])) {
            $security_token = '';
        }

        if ($security_token) {
            $_POST['token'] = $security_token;
            $functions = new Common_controller($_POST);
            $response = call_user_func(array($functions, $_POST['request_method']), $_POST);
            echo json_encode($response);
            exit;
        } else {
            $is_bad_req++;
            $response['message'] = "Opps! Your request is not authenticated";
        }
    } else {
        $is_bad_req++;
    }
} catch(\Exception $ex) {
    $is_bad_req++;
}

if ($is_bad_req) {
    echo json_encode($response);
    exit;
}