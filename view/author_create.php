<?php
include_once('header.php');
?>

<section class="vh-100 gradient-custom" xmlns="http://www.w3.org/1999/html">
    <div class="container py-5 ">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-lg-9 col-xl-7">
                <div class="card shadow-2-strong card-registration rounded-3">
                    <div class="card-body p-4 p-md-5">
                        <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Create Author</h3>
                        <form id="addAuthor" action="" method="post">
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <label class="form-label" for="firstName">First Name</label>
                                        <input type="text" name="first_name" id="firstName" required class="form-control form-control-lg" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <label class="form-label" for="lastName">Last Name</label>
                                        <input type="text" name="last_name" id="lastName" class="form-control form-control-lg" required/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-4 d-flex align-items-center">
                                    <div class="form-outline datepicker w-100">
                                        <label for="birthdayDate" class="form-label">Birthday</label>
                                        <input type="date" name="birthday"  class="form-control form-control-lg" id="birthdayDate" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4 d-flex align-items-center">
                                    <div class="form-outline w-100">
                                        <label for="biography" class="form-label">Biography</label>
                                        <input type="text" name="biography" class="form-control form-control-lg" id="biography" required/>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <label class="form-label" for="placeOfBirth">Place of birth</label>
                                        <input type="tel" name="place_of_birth" id="placeOfBirth" class="form-control form-control-lg" required/>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <h6 class="mb-2 pb-1">Gender: </h6>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="femaleGender">Female</label>
                                        <input class="form-check-input" type="radio" name="gender" id="femaleGender" value="female" checked />
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label" for="maleGender">Male</label>
                                        <input class="form-check-input" type="radio" name="gender" id="maleGender" value="male" />
                                    </div>
                                </div>
                            </div>
                            <div class="mt-2 pt-2 text-center">
                                <button id="authorAddBtn" class="btn btn-primary btn-lg" value="Add Author" />Add Author</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    (function () {
        verifyLogin();
    })();
</script>
</body>
</html>
