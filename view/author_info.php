<?php
include_once('header.php');
?>

<section>
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-4">
                <h2 class="text-center">Author Details</h2>
                <div class="card mb-4">
                    <div class="card-body text-center">
                        <img src="<?php echo SITE_URL.'assets/img/user.webp'; ?>" alt="avatar" class="rounded-circle img-fluid" style="width: 150px;">
                        <h5 class="my-3">John Smith</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">First Name</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0" id="firstName">-</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">Last Name</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0" id="lastName">-</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">Birthdate</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0" id="birthDate">-</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">Gender</p>
                            </div>
                            <div class="col-sm-9">
                                    <p class="text-muted mb-0" id="gender">-</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">Place Of Birth</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0" id="placeOfBirth">-</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">Biography</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0" id="biography">-</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <h2 class="">Books Details</h2>
            <div class="px-3">
                <table class="table table-bordered text-center px-2">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">ISBN</th>
                        <th scope="col">Format</th>
                        <th scope="col">Number of pages</th>
                        <th scope="col">Release date</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="bookListTable"></tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    (function () {
        verifyLogin('getAuthorDetails()');
    })();
</script>

</body>
</html>
