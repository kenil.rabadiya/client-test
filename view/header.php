<?php
include_once '../config/config.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo SITE_URL.'assets/css/bootstrap.min.css'; ?>" rel="stylesheet">
    <link href="<?php echo SITE_URL.'assets/css/common.css'; ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo SITE_URL.'assets/css/font-family.css'; ?>">
    <link href="<?php echo SITE_URL.'assets/css/toastr.css'; ?>" rel="stylesheet">
    <link href="<?php echo SITE_URL.'assets/css/select2.min.css'; ?>" rel="stylesheet">

    <script src="<?php echo SITE_URL.'assets/js/jquery.min.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/custom.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/toastr.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/select2.min.js'; ?>"></script>

    <title>Royal Apps</title>
</head>
<body>
<div class="header-container">
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="<?php echo SITE_URL.'assets/img/user.webp'; ?>" alt="Logo" style="width:40px;" class="rounded-pill">
                <span class="form-label" id="userFullName">...</span>
            </a>
            <a class="align-items-end navbar-brand btn" onclick="doLogOut()">
                <span class="form-label">Logout</span>
            </a>
        </div>
    </nav>
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item">
                        <button id="authorListBtn" type="button" class="btn" onclick="window.location.href='index.php'">Author List</button>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
