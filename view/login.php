<?php
include_once '../config/config.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo SITE_URL.'assets/css/bootstrap.min.css'; ?>" rel="stylesheet">
    <link href="<?php echo SITE_URL.'assets/css/login.css'; ?>" rel="stylesheet">
    <link href="<?php echo SITE_URL.'assets/css/toastr.css'; ?>" rel="stylesheet">

    <script src="<?php echo SITE_URL.'assets/js/jquery.min.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/custom.js'; ?>"></script>
    <script src="<?php echo SITE_URL.'assets/js/toastr.js'; ?>"></script>

    <title>Login</title>
</head>
<body>

<div class="wrapper">
    <div class="logo">
        <img src="<?php echo SITE_URL.'assets/img/user.webp'; ?>" alt="">
    </div>
    <div class="text-center mt-4 name">Royal Apps</div>
    <form class="p-3 mt-3" id="loginForm" action="" method="post">
        <div class="form-field d-flex align-items-center">
            <span class="far fa-user"></span>
            <input type="email" name="email" id="email" placeholder="Email" required>
        </div>
        <div class="form-field d-flex align-items-center">
            <span class="fas fa-key"></span>
            <input type="password" name="password" id="password" placeholder="Password" required>
        </div>
        <button class="btn mt-3" id="loginSubmit">Login</button>
    </form>
</div>
</body>
</html>
