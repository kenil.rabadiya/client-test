<?php
include_once('header.php');
?>

<section class="vh-100 bg-image" xmlns="http://www.w3.org/1999/html">
    <div class="mask d-flex align-items-center gradient-custom-3">
        <div class="container ">
            <div class="row d-flex justify-content-center align-items-center ">
                <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                    <div class="card rounded-3">
                        <div class="card-body p-5">
                            <h2 class="text-uppercase text-center mb-5">Create Book</h2>
                            <form id="createBook" action="" method="post">
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="authorNameList">Author</label>
                                    <select type="text" name="author_id" id="authorNameList" class="form-control form-control-lg"  required/>
                                    </select>
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="title">Title</label>
                                    <input type="text" id="title" name="title" class="form-control form-control-lg" required />
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="release_date">Release Date</label>
                                    <input type="date" id="release_date" name="release_date" class="form-control form-control-lg" required />
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="description">Description</label>
                                    <input type="text" id="description" name="description"  class="form-control form-control-lg" required />
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="isbn">Isbn</label>
                                    <input type="text" id="isbn" name="isbn"  class="form-control form-control-lg" required />
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="format">Format</label>
                                    <input type="text" id="format" name="format" class="form-control form-control-lg" required />
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="numberOfPage">Number Of Pages</label>
                                    <input type="number" id="numberOfPage" name="number_of_pages" class="form-control form-control-lg" required />
                                </div>

                                <div class="d-flex justify-content-center">
                                    <button id="createBookBtn" class="btn btn-success btn-block btn-lg gradient-custom-4 ">Create Book</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    (function () {
        verifyLogin('getAuthorNameList()');
    })();
</script>
</body>
</html>
