<?php
include_once('header.php');
?>

<div class="container">
    <div class="row mt-5">
        <h2 class="mb-2 ps-0 col-6">Author List</h2>
        <div class="col-6 text-end pe-0">
            <button type="button" class="btn btn-dark" onclick="window.location.href='author_create.php'">Add Author</button>
            <button type="button" class="btn btn-dark" onclick="window.location.href='book_create.php'">Add Book</button>
        </div>
    </div>
    <div class="row">
        <input placeholder="Search" id="searchAuthor" value="">
        <table class="table table-bordered text-center">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Full Name</th>
                <th scope="col">Place of Birth</th>
                <th scope="col">Gender</th>
                <th scope="col">Birthday</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody id="authorTableBody"></tbody>
        </table>
        <nav aria-label="Page navigation example" style="display: none;" id="authorTablePaginationNav">
            <ul class="pagination justify-content-center" id="authorTablePagination">

            </ul>
        </nav>
    </div>
</div>
</body>
</html>

<script>
    (function () {
        verifyLogin('showAuthorList()');
    })();
</script>