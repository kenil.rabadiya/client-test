<?php
ob_start();
define('ABS_PATH', dirname(dirname(__FILE__)));
define('DOMAIN_NAME', $_SERVER['SERVER_NAME'].'/client-test/');
define('SITE_URL', 'http://' . DOMAIN_NAME);
define('API_URL', 'https://candidate-testing.api.royal-apps.io/api/v2');

define('STATUS_OK', '200');
define('STATUS_DELETE', '204');

define('PER_PAGE_RECORD', '10');

?>

<script>
    const __config = {
        apiUrl: '<?= API_URL ?>',
        ajaxUrl: '<?= SITE_URL ?>view/ajax.php'
    };
</script>

