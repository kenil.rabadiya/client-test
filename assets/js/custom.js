$(document).ready(function (){
    $("#loginForm").submit(function(e) {
        e.preventDefault();
        $('#loginSubmit').attr('disabled', true);
        let email = $('#email').val();
        let password = $('#password').val();

        $.ajax({
            url: __config.apiUrl + '/token',
            type: 'post',
            dataType: "json",
            data: JSON.stringify({ email: email, password: password }),
            success: function(data) {
                toastr.success('Your login was successful');
                let authData = {token: data.token_key};
                setCookie('authData', btoa(JSON.stringify(authData)), data.expires_at);
                setTimeout(function (){
                    window.location = '../view/index.php'
                },1000);
            },
            error: function (error) {
                $('#loginSubmit').attr('disabled', false);
                toastr.error('Enter valid credentials');
            }
        });

    });

    $("#addAuthor").submit(function(e) {
        e.preventDefault();
        $('#authorAddBtn').attr('disabled', true);
        let frmData = $(this).serialize();
        frmData += '&' + $.param({request_method: 'create_new_author'});
        $.ajax({
            url: __config.ajaxUrl,
            type: 'post',
            dataType: "json",
            data: frmData,
            success: function(response) {
                if(response.result == 'true') {
                    toastr.success(response.message);
                    setTimeout(function (){
                        $('#authorListBtn').trigger('click');
                    },500)
                }
            },
            error: function (error) {
                $('#authorAddBtn').attr('disabled', false);
                toastr.error(error);
            }
        });

    });

    $("#createBook").submit(function(e) {
        e.preventDefault();
        $('#createBookBtn').attr('disabled', true);
        let frmData = $(this).serialize();
        frmData += '&' + $.param({request_method: 'create_new_book'});
        $.ajax({
            url: __config.ajaxUrl,
            type: 'post',
            dataType: "json",
            data: frmData,
            success: function(response) {
                if(response.result == 'true') {
                    toastr.success(response.message);
                    setTimeout(function (){
                        window.location = '../view/';
                    },500)
                }
            },
            error: function (error) {
                $('#createBookBtn').attr('disabled', false);
                toastr.error(error);
            }
        });

    });

    $(document).on('keyup', '#searchAuthor', function() {
        showAuthorList();
    });
});

const getParamName = (name, url) => {
    url = url == '' ? window.location.search : url;
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

const setCookie = (name, value, expiryTime = '') =>  {
    var expires = "";
    if (expiryTime) {
        let dateTime = new Date(expiryTime);
        expires = "; expires=" + dateTime.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

const verifyLogin = (callBackFn = false) => {
    let isLoginFail = false;
    let authData = getCookie('authData');
    if(authData) {
        authData = JSON.parse(atob(authData));
        authData.request_method = 'get_user_data';
        $.ajax({
            url: __config.ajaxUrl,
            type: 'post',
            dataType: "json",
            data: authData,
            success: function(response) {
                if(response.result == 'true') {
                    showUserData(response.user_data);
                    /* Set authentication token */
                    $.ajaxSetup({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authentication", authData.token);
                        }
                    });
                    callBackFn && eval(callBackFn);
                } else {
                    isLoginFail = true;
                }
            },
            error: function (error) {
                isLoginFail = true;
            }
        });
    } else {
        isLoginFail = true;
    }

    if(isLoginFail) {
        toastr.error('Login required!!');
        setTimeout(function() {
            window.location = '../view/login.php';
        }, 1000);
    }
}

const showUserData = (userData) => {
    $('#userFullName').text(userData.first_name + ' ' + userData.last_name);
}

const doLogOut = () => {
    document.cookie = "authData=; expires=0;path=/;"
    toastr.success('Logout success!!');
    setTimeout(function() {
        window.location = '../view/login.php';
    }, 1000);
}

let authorListAjax = null;
const showAuthorList = (pageNo = 1) => {
    if (authorListAjax && authorListAjax.readyState != 4) {
        authorListAjax.abort();
    }

    let searchQuery = $('#searchAuthor').val();
    authorListAjax = $.ajax({
        url: __config.ajaxUrl,
        type: 'post',
        dataType: "json",
        data: { page_number: pageNo, request_method: 'show_author_list', query: searchQuery },
        success: function(response) {
            if(response.result == 'true') {
                $('#authorTableBody').html(response.tableHtml);
                $('#authorTablePagination').html(response.paginationHtml);
                if(response.paginationHtml) {
                    $('#authorTablePaginationNav').show();
                } else {
                    $('#authorTablePaginationNav').hide();
                }
            } else {
                toastr.error(response.message);
            }
        },
        error: function (error) { }
    });
}

const getAuthorNameList = () => {
    $('#authorNameList').select2({
        placeholder: "Search Author",
        ajax: {
            url: __config.ajaxUrl,
            type: 'post',
            dataType: "json",
            data: function (params) {
                return {
                    search_keyword: params.term || "", /* search term */
                    request_method: 'get_author_name_list'
                };
            },
            processResults: function (data, params) {
                console.log(data.authorNameList)
                return {
                    results: data.authorNameList,
                };
            }
        }
    });
}

const deleteAuthor = (authorId) => {
    $.ajax({
        url: __config.ajaxUrl,
        type: 'post',
        dataType: "json",
        data: { author_id: authorId, request_method: 'verify_author_books' },
        success: function(response) {
            if(response.result == 'true') {
                if(response.canDelete) {
                    toastr.warning("<button type='button' id='confirmationButtonYes' class='btn'>Yes</button>",'Delete Author ?',
                        {
                            closeButton: false,
                            allowHtml: true,
                            onShown: function (toast) {
                                $("#confirmationButtonYes").click(function(){
                                    $.ajax({
                                        url: __config.ajaxUrl,
                                        type: 'post',
                                        dataType: "json",
                                        data: { author_id: authorId, request_method: 'delete_author' },
                                        success: function(response) {
                                            if(response.result == 'true') {
                                                toastr.success('Author deleted successfully');
                                                showAuthorList();
                                            } else {
                                                toastr.error(response.message);
                                            }
                                        },
                                        error: function (error) { }
                                    });
                                });
                            }
                        });
                } else {
                    toastr.warning('Author has one or more books. Please delete all books to delete author.');
                }
            } else {
                toastr.error(response.message);
            }
        },
        error: function (error) { }
    });
}

const getAuthorDetails = () => {
    let authorId = getParamName('author_id', window.location.href);
    if(authorId) {
        $.ajax({
            url: __config.ajaxUrl,
            type: 'post',
            dataType: "json",
            data: { author_id: authorId, request_method: 'get_author_details' },
            success: function(response) {
                if(response.result == 'true') {
                    $('#bookListTable').html(response.tableHtml);
                    let authorData = response.authorData;
                    $('#firstName').html(authorData.first_name);
                    $('#lastName').html(authorData.last_name);
                    $('#birthDate').html(authorData.birthday);
                    $('#gender').html(authorData.gender);
                    $('#placeOfBirth').html(authorData.place_of_birth);
                    $('#biography').html(authorData.biography);
                } else {
                    toastr.error(response.message);
                    setTimeout(function() {
                        window.location = '../view/';
                    }, 1000);
                }
            },
            error: function (error) { }
        });
    } else {
        toastr.error('Author Id required!!');
        setTimeout(function() {
            window.location = '../view/';
        }, 1000);
    }
}

const deleteBook = (bookId) => {
    toastr.warning("<button type='button' id='confirmationButtonYes' class='btn'>Yes</button>",'Delete Book ?',
    {
        closeButton: true,
        allowHtml: true,
        onShown: function (toast) {
            $("#confirmationButtonYes").click(function(){
                $.ajax({
                    url: __config.ajaxUrl,
                    type: 'post',
                    dataType: "json",
                    data: { book_id: bookId, request_method: 'delete_book' },
                    success: function(response) {
                        if(response.result == 'true') {
                            toastr.success('Book deleted successfully');
                            getAuthorDetails();
                        } else {
                            toastr.error(response.message);
                        }
                    },
                    error: function (error) { }
                });
            });
        }
    });
}
